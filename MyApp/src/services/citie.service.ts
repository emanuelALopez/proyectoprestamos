import {Injectable} from '@angular/core';
@Injectable()
export class CitieService{
    cities = [
        {id:1,name: 'Pereira'},
        {id:3,name: 'Barranquilla'},
        {id:2,name: 'Bogotá'}
    ];

    public getCities(){
        return this.cities;
    }
    public getCitie(id){
        return this.cities.filter((e)=>{return e.id==id})[0] || {
            id:null,name:null
        };
    }

    agregarCity(city){
        this.cities.push(city);
    }
}