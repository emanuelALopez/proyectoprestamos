import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {CitieService} from '../../services/citie.service';

/**
 * Generated class for the DetalleCiudadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-ciudad',
  templateUrl: 'detalle-ciudad.html',
})
export class DetalleCiudadPage {
  city = {
    id: null, name: null
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public cities: CitieService,public control: AlertController) {
    let id = navParams.get('id');
    if(this.id!=0){
      this.city = cities.getCitie(id);
    }
  }
  public createCity(){
    this.city.id = Date.now();
    this.cities.agregarCity(this.city);
    this.alerta();
  }
  public alerta(){
    let alert = this.control.create({
      title: 'Ciudad creada',
      message: 'Se ha creado satisfactoriamente la ciudad',
      buttons: [
        {
          text:'OK',
          handler: ()=>{
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present()

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleCiudadPage');
  }

}
