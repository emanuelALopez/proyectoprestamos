import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleCiudadPage } from './detalle-ciudad';

@NgModule({
  declarations: [
    DetalleCiudadPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleCiudadPage),
  ],
})
export class DetalleCiudadPageModule {}
