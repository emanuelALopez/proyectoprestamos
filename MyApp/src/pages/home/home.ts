import { DetalleCiudadPage } from './../detalle-ciudad/detalle-ciudad';
import { CitieService } from './../../services/citie.service';
import { Component,ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cities = [];
  constructor(public navCtrl: NavController,public ciudades:CitieService) {
    this.cities = ciudades.getCities();
  }
  public muestraPagina(id){
    this.navCtrl.push(DetalleCiudadPage,{id:id});
  }

  public crearCiudad(){
    this.navCtrl.push(DetalleCiudadPage,{id:0})
  }
}